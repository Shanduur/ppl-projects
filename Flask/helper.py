import sqlite3

def helper():
    DATABASE = 'database/lab.db'

    conn = sqlite3.connect(DATABASE)
    try:
        conn.execute('DROP TABLE users')
    except Exception as e:
        print(e)

    try:
        conn.execute('DROP TABLE books')
    except Exception as e:
        print(e)

    conn.execute('''
        CREATE TABLE users (
            id INTEGER PRIMARY KEY,
            username TEXT, 
            password TEXT, 
            admin BOOL DEFAULT FALSE)
        ''')
    conn.execute('''
        CREATE UNIQUE INDEX up ON users(username)
        ''')
    conn.execute('''
        INSERT INTO users(username, password, admin)
        VALUES ('root', 'toor', true)''')
    conn.commit()
    conn.close()

    conn = sqlite3.connect(DATABASE)
    conn.execute('''
        CREATE TABLE books (
            id INTEGER PRIMARY KEY,
            title TEXT, 
            author TEXT)
        ''')
    conn.execute('''
        CREATE UNIQUE INDEX ta ON books(title, author)
        ''')
    conn.execute('''
        INSERT INTO books(title, author)
        VALUES ('example, p.1', 'ex ample')
        ''')
    conn.execute('''
        INSERT INTO books(title, author) 
        VALUES ('example, p.2', 'ex ample')
        ''')
    conn.commit()

    cur = conn.cursor()
    cur.execute('SELECT * FROM users')
    users = cur.fetchall()
    cur.execute('SELECT * FROM books')
    books = cur.fetchall()
    cur.close()
    conn.close()

    print(users)
    print(books)

if __name__ == "__main__":
    helper()