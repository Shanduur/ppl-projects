from flask import Flask
from flask import render_template, request, redirect, url_for, flash, session
from flask_session import Session
import sqlite3

app = Flask('Flask - Lab')
sess = Session()

DATABASE = 'database/lab.db'

@app.route('/')
def index(**kwargs):
    content = kwargs.get('content')
    if not content:
        content = ''
    name = ''
    admin = False
    con = sqlite3.connect(DATABASE)
    cur = con.cursor()
    cur.execute('''
        SELECT * 
        FROM books
        ORDER BY author DESC
        ''')
    books = cur.fetchall()
    cur.close()
    con.close()

    if 'user' in session:
        name = session['user']

    if 'admin' in session:
        admin = session['admin']

    return render_template('index.html', books = books, name = name, admin = admin, content = content)

@app.route('/onBookAdd', methods=['POST'])
def onBookAdd():
    if 'user' in session:
        title = request.form['title']
        author = request.form['author']

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute('INSERT INTO books (title,author) VALUES (?,?)', (title,author))
            con.commit()
        except Exception as e:
            con.rollback()
            redirect("/", code=302)
            return index(content='Error creating new user: %s' % e)
        con.close()

        return index(content='Created succesfully!')
    
    else:
        return redirect("/", code=302)

@app.route('/user', methods=['GET', 'POST'])
def user():
    if 'user' in session:
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute('SELECT * FROM users WHERE username = ?', (session['user'],))
        user = cur.fetchall()
        cur.close()
        con.close()

        return render_template('user.html', user = user)
    
    else:
        return redirect("/", code=302)

#======= ADMIN =======
@app.route('/admin', methods=['GET', 'POST'])
def admin(**kwargs):
    if 'admin' in session:
        content = kwargs.get('content')
        if not content:
            content = ''
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        cur.execute('SELECT * FROM users')
        users = cur.fetchall()
        cur.close()
        con.close()

        return render_template('admin.html', content = content, users = users)
    
    else:
        return redirect("/", code=302)

@app.route('/onUserAdd', methods=['POST'])
def onAdd():
    if 'admin' in session:
        login = request.form['login']
        password = request.form['password']

        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute('INSERT INTO users (username, password) VALUES (?,?)', (login,password))
            con.commit()
        except Exception as e:
            con.rollback()
            return admin(content='Error creating new user: %s' % e)
        con.close()

        return admin(content='Created succesfully!')
    
    else:
        return redirect("/", code=302)

@app.route('/onBookDelete', methods=['POST'])
def onBookDelete():
    if 'admin' in session:
        id = request.form['id']
        con = sqlite3.connect(DATABASE)
        try:
            con.execute('DELETE FROM books WHERE id = ?', (id,))
            con.commit()
        except Exception as e:
            con.rollback()
            return index(content='Error removing book: %s' % e)
        con.close()

        return index(content='Removed succesfully!')
    
    else:
        return redirect("/", code=302)

@app.route('/onUserDelete', methods=['POST'])
def onUserDelete():
    if 'admin' in session:
        try:
            id = int(request.form['id'])
        except ValueError:
            id = 0
        if id == 1:
            return admin(content='Cannot remove admin')
        con = sqlite3.connect(DATABASE)
        try:
            con.execute('DELETE FROM users WHERE id = ?', (id,))
            con.commit()
        except Exception as e:
            con.rollback()
            return admin(content='Error removing user: %s' % e)
        con.close()

        return admin(content='Removed succesfully!')
    
    else:
        return redirect("/", code=302)

#======= LOGIN =======
@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/onLogin', methods=['POST'])
def onLogin():
    login = request.form['login']
    password = request.form['password']
    con = sqlite3.connect(DATABASE)
    cur = con.execute('SELECT COUNT(*) FROM users WHERE username = ? AND password = ?', (login, password))

    for c in cur:
        if c[0] == 1:
            session['user'] = login
            cur2 = con.execute('SELECT admin FROM users WHERE username = ? AND password = ?', (login, password))
            for c2 in cur2:
                if c2[0]:
                    session['admin'] = c2[0]
        else:
            return "Incorrect credentials <br><a href='/'>Back</a>"
    
    con.close()
    return "Logged in <br><a href='/'>Back</a>"

@app.route('/onLogout', methods=['GET'])
def onLogoff():
    if 'user' in session:
        session.pop('user')
        if 'admin' in session:
            session.pop('admin')
    else:
        if 'admin' in session:
            session.pop('admin')
    
    return "Logged out <br><a href='/'>Back</a>"

app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'
sess.init_app(app)
app.config.from_object(__name__)
app.debug = True
app.run()