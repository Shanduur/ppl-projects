import pygame
pygame.init()
pygame.font.init()

import entity
import time

K_UP = pygame.K_w
K_DOWN = pygame.K_s
K_RIGHT = pygame.K_d
K_LEFT = pygame.K_a

MAX_ENEMIES = 7
MAX_OBSTACLES = 4

def fill(surface, color):
    """
    Fill all pixels of the surface with color, preserve transparency.
    """
    w, h = surface.get_size()
    r, g, b, _ = color
    for x in range(w):
        for y in range(h):
            a = surface.get_at((x, y))[3]
            surface.set_at((x, y), pygame.Color(r, g, b, a))

class Scene:
    """
    Scene class
    """

    WINDOW_WIDTH = 600
    WINDOW_HEIGHT = 700

    __move_left = False
    __move_right = False
    __move_up = False
    __move_down = False

    window = None
    background = None
    font = None

    max_enemies = MAX_ENEMIES
    enemies = []
    max_obstacles = MAX_OBSTACLES
    obstacles = []
    player = None
    health = []
    bullets = []

    time_now = None
    clock = None

    points = 0

    def __init__(self):
        self.window = pygame.display.set_mode((self.WINDOW_WIDTH, self.WINDOW_HEIGHT))

        pygame.display.set_caption('2742')

        self.background = entity.Background()
        self.background.image = pygame.transform.scale(
            self.background.image,
            (self.WINDOW_WIDTH, self.WINDOW_HEIGHT))

        self.player = entity.Player((self.WINDOW_WIDTH/2, self.WINDOW_HEIGHT/2))

        for _ in range (0, 5):
            self.health.append(pygame.transform.scale(entity.PLAYER_TEXTURE, (25,25)))

        self.clock = pygame.time.Clock()
        self.time_now = self.clock.tick()

        self.font = pygame.font.Font('./resources/major.ttf', 50)

    def events(self, state):
        """
        handling of events
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                state = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    state = False

                if event.key == K_UP:
                    self.__move_up = True
                if event.key == K_LEFT:
                    self.__move_left = True
                if event.key == K_DOWN:
                    self.__move_down = True
                if event.key == K_RIGHT:
                    self.__move_right = True

            if event.type == pygame.KEYUP:
                if event.key == K_UP:
                    self.__move_up = False
                if event.key == K_LEFT:
                    self.__move_left = False
                if event.key == K_DOWN:
                    self.__move_down = False
                if event.key == K_RIGHT:
                    self.__move_right = False

        if self.__move_up:
            self.player.rect.y -= self.player.speed

        if self.__move_left:
            self.player.rect.x -= self.player.speed

        if self.__move_down:
            self.player.rect.y += self.player.speed

        if self.__move_right:
            self.player.rect.x += self.player.speed

        return state

    def loop(self):
        """
        main logic of game
        """
        tick = self.clock.tick()
        if self.player.rect.x < 0 - self.player.rect.width/2:
            self.player.rect.x = 0 - self.player.rect.width/2

        if self.player.rect.x > self.WINDOW_WIDTH - self.player.rect.width/2:
            self.player.rect.x = self.WINDOW_WIDTH - self.player.rect.width/2

        if self.player.rect.y < 0 - self.player.rect.height/2:
            self.player.rect.y = 0 - self.player.rect.height/2

        if self.player.rect.y > self.WINDOW_HEIGHT - self.player.rect.height/2:
            self.player.rect.y = self.WINDOW_HEIGHT - self.player.rect.height/2

        while len(self.enemies) < self.max_enemies:
            self.enemies.append(entity.Enemy(self.WINDOW_WIDTH))

        while len(self.obstacles) < self.max_obstacles:
            self.obstacles.append(entity.Obstacle(self.WINDOW_WIDTH))

        to_del = []

        for i in range(0, len(self.enemies)):
            self.enemies[i].rect.y += self.enemies[i].speed
            if self.enemies[i].rect.y > self.WINDOW_HEIGHT:
                to_del.append(i)

        to_del.sort(reverse=True)

        for i in range(0, len(to_del)):
            del self.enemies[to_del[i]]

        to_del.clear()

        for i in range(0, len(self.obstacles)):
            self.obstacles[i].rect.y += self.obstacles[i].speed
            if self.obstacles[i].rect.y > self.WINDOW_HEIGHT:
                to_del.append(i)

        to_del.sort(reverse=True)

        for i in range(0, len(to_del)):
            del self.obstacles[to_del[i]]

        to_del.clear()

        self.time_now += tick
        if self.time_now > 120:
            self.bullets.append(entity.Bullet(self.player.center()))
            self.time_now = 0

        for i in range(0, len(self.bullets)):
            self.bullets[i].rect.y -= self.bullets[i].speed
            if self.bullets[i].rect.y < -50:
                to_del.append(i)

        to_del.sort(reverse=True)

        for i in range(0, len(to_del)):
            del self.bullets[to_del[i]]

        to_del.clear()

        if self.player.immunity_time <= 0:
            if self.player.during_protection:
                self.player.during_protection = False
                self.player.reset()

            i, val = self.player.collision(self.obstacles)
            if val:
                del self.obstacles[i]
                self.player.health -= 1
                if self.player.health < 0:
                    exit(1)
                self.player.immunity_time = 300
                fill(self.health[self.player.health], (192, 192, 192, 0))
                self.player.during_protection = True
            i, val = self.player.collision(self.enemies)
            if val:
                del self.enemies[i]
                self.player.health -= 1
                if self.player.health < 0:
                    exit(1)
                self.player.immunity_time = 500
                fill(self.health[self.player.health], (192, 192, 192, 0))
                self.player.during_protection = True

        else:
            self.player.immunity_time -= tick

        to_del2 = []
        to_del3 = []

        for i in range(0, len(self.bullets)):
            j, val = self.bullets[i].collision(self.enemies)
            if val:
                to_del.append(i)
                self.enemies[j].health -= 1
                if self.enemies[j].health <= 0:
                    to_del2.append(j)
                    self.points += self.enemies[j].mutation * 100
            else:
                j, val = self.bullets[i].collision(self.obstacles)
                if val:
                    to_del.append(i)
                    self.obstacles[j].health -= 1
                    if self.obstacles[j].health <= 0:
                        to_del3.append(j)
                        self.points += self.obstacles[j].mutation * 50

        to_del.sort(reverse=True)
        to_del2.sort(reverse=True)
        to_del3.sort(reverse=True)

        for i in range(0, len(to_del)):
            del self.bullets[to_del[i]]

        for i in range(0, len(to_del2)):
            del self.enemies[to_del2[i]]

        for i in range(0, len(to_del3)):
            del self.obstacles[to_del3[i]]

        to_del.clear()
        to_del2.clear()
        to_del3.clear()


    def render(self):
        """
        rendering stuff
        """
        self.window.fill([0, 0, 2])
        self.window.blit(self.background.image, self.background.rect)
        self.window.blit(self.player.image, self.player.rect)

        x = 10
        y = 10
        for i in range(0, 5):
            self.window.blit(self.health[i], (x, y))
            x += 25

        for i in range(0, len(self.obstacles)):
            self.window.blit(self.obstacles[i].image, self.obstacles[i].rect)

        for i in range(0, len(self.enemies)):
            self.window.blit(self.enemies[i].image, self.enemies[i].rect)

        for i in range(0, len(self.bullets)):
            self.window.blit(self.bullets[i].image, self.bullets[i].rect)

        textsurface = self.font.render(str(self.points), False, (255, 255, 255))

        self.window.blit(textsurface,
            (self.WINDOW_WIDTH-textsurface.get_rect().width-10,0))

class App:
    """
    application class
    """
    scene = None
    running = True

    def __init__(self):
        self.scene = Scene()

    def execute(self):
        """
        main loop of application
        """
        while self.running:
            self.running = self.scene.events(self.running)
            self.scene.loop()
            self.scene.render()
            pygame.display.update()

if __name__ == "__main__":
    app = App()
    app.execute()
