import pygame
import random

pygame.init()

class Entity(pygame.sprite.Sprite):
    health = None
    speed = None
    mutation = None
    velocity = None
    clock = None
    immunity_time = 0

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.clock = pygame.time.Clock()

    def collision(self, entities):
        for i in range(0, len(entities)):
            if self.rect.colliderect(entities[i].rect):
                return (i, True)

        return (-1, False)

BACKGROUND_TEXTURE = pygame.image.load("./resources/background.png")

PLAYER_TEXTURE = pygame.image.load("./resources/spaceships/spaceship_1.png")
BULLET_TEXTURE = pygame.image.load("./resources/bullets/bullet.png")

ENEMY_TEXTURE_1 = pygame.image.load("./resources/spaceships/ufo_1.png")
ENEMY_TEXTURE_2 = pygame.image.load("./resources/spaceships/ufo_2.png")
ENEMY_TEXTURE_3 = pygame.image.load("./resources/spaceships/ufo_3.png")

OBSTACLE_TEXTURE_1 = pygame.image.load("./resources/obstacles/asteroid_1.png")
OBSTACLE_TEXTURE_2 = pygame.image.load("./resources/obstacles/asteroid_2.png")
OBSTACLE_TEXTURE_3 = pygame.image.load("./resources/obstacles/asteroid_3.png")

class Bullet(Entity):
    """
    Bullet
    """
    def __init__(self, position):
        Entity.__init__(self)
        self.image = BULLET_TEXTURE
        self.rect = self.image.get_rect(center=position)
        self.speed = 10

class Player(Entity):
    """
    Player
    """

    during_protection = False
    
    def __init__(self, position):
        Entity.__init__(self)
        self.image = PLAYER_TEXTURE
        self.rect = self.image.get_rect(center=position)
        self.health = 5
        self.speed = 6

    def center(self):
        """
        center
        """
        return (self.rect.x + self.rect.width/2,
            self.rect.y + self.rect.height/2)

    def reset(self):
        """
        reset
        """
        self.image = PLAYER_TEXTURE

class Enemy(Entity):
    """
    Enemy
    """

    def __init__(self, window_width):
        Entity.__init__(self)

        self.mutation = random.randrange(1, 4)
        if self.mutation == 1:
            self.image = ENEMY_TEXTURE_1
            self.health = 4
            self.speed = 1
        elif self.mutation == 2:
            self.image = ENEMY_TEXTURE_2
            self.health = 3
            self.speed = 2
        elif self.mutation == 3:
            self.image = ENEMY_TEXTURE_3
            self.health = 8
            self.speed = 1

        pos_x = random.randrange(0, window_width)

        self.rect = self.image.get_rect(center=(pos_x,0))

class Obstacle(Entity):
    """
    Obstacle - asteroid
    """

    def __init__(self, window_width):
        Entity.__init__(self)
        self.mutation = random.randrange(1, 4)

        if self.mutation == 1:
            self.image = OBSTACLE_TEXTURE_1
            self.health = 10
            self.speed = 1
        elif self.mutation == 2:
            self.image = OBSTACLE_TEXTURE_2
            self.health = 20
            self.speed = 3
        elif self.mutation == 3:
            self.image = OBSTACLE_TEXTURE_3
            self.health = 12
            self.speed = 1

        pos_x = random.randrange(0, window_width)

        self.rect = self.image.get_rect(center=(pos_x,0))


class Background(pygame.sprite.Sprite):
    """
    Background
    """
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = BACKGROUND_TEXTURE
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = (0, 0)