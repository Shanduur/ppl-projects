from panda3d.core import loadPrcFile
loadPrcFile('config/conf.prc')

from panda3d.core import CollisionNode, CollisionFloorMesh, CollisionHandlerPusher
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor

keyMap = {
    'up': False,
    'down': False,
    'left': False,
    'right': False
}

flags = {
    'state1': False,
    'state2': False,
    'state3': True,
    'moving': False,
    'rotating': False,
}

def updateKeyMap(key: str, state: bool):
    keyMap[key] = state

class Game(ShowBase):
    """
    Main class of our game
    """
    def __init__(self):
        super().__init__()

        # diables default camera controls
        self.disableMouse()

        self.envSound = self.loader.loadSfx("./resources/forest.ogg")
        self.envSound.play()

        self.wood = self.loader.loadSfx("./resources/wood.ogg")
        self.wood.setVolume(0.08)

        self.wood2 = self.loader.loadSfx("./resources/wood2.ogg")
        self.wood2.setVolume(0.08)

        self.env = self.loader.loadModel('models/environment')
        self.env.reparentTo(self.render)

        self.player = Actor('resources/PandaSampleModels/SimpleEnemy/simpleEnemy',
            {
                'walk':'resources/PandaSampleModels/SimpleEnemy/simpleEnemy-walk',
                'stand':'resources/PandaSampleModels/SimpleEnemy/simpleEnemy-stand',
            })
        self.player.setScale(10.0, 10.0, 10.0)
        self.player.reparentTo(self.render)

        self.accept('arrow_left', updateKeyMap, ['left', True])
        self.accept('arrow_left-up', updateKeyMap, ['left', False])

        self.accept('arrow_right', updateKeyMap, ['right', True])
        self.accept('arrow_right-up', updateKeyMap, ['right', False])

        self.accept('arrow_up', updateKeyMap, ['up', True])
        self.accept('arrow_up-up', updateKeyMap, ['up', False])

        self.accept('arrow_down', updateKeyMap, ['down', True])
        self.accept('arrow_down-up', updateKeyMap, ['down', False])

        self.player.loop('walk')
        self.player.setPlayRate(2.0, 'walk')

        self.speed = 25
        self.rotationSpeed = 90

        self.taskMgr.add(self.updatePlayer, 'updatePlayer', priority=10)
        self.taskMgr.add(self.followPlayer, 'followPlayer', priority=20)
        self.taskMgr.add(self.animate, 'animate', priority=30)
        self.taskMgr.add(self.soundTask, 'soundTask', priority=50)

    def animate(self, task):
        if keyMap['up']:
            if not flags['state1']:
                self.player.setPlayRate(2.0, 'walk')
                self.player.loop('walk')
                flags['state1'] = True
                flags['state2'] = False
                flags['state3'] = False
                flags['moving'] = True
        elif keyMap['down']:
            if not flags['state2']:
                self.player.setPlayRate(-2.0, 'walk')
                self.player.loop('walk')
                flags['state1'] = False
                flags['state2'] = True
                flags['state3'] = False
                flags['moving'] = True
        else:
            if not flags['state3']:
                self.player.loop('stand')
                flags['state1'] = False
                flags['state2'] = False
                flags['state3'] = True
                flags['moving'] = False

        if keyMap['left'] or keyMap['right']:
            flags['rotating'] = True
        else:
            flags['rotating'] = False

        return task.cont

    def updatePlayer(self, task):
        """
        Update
        """

        dt = globalClock.getDt()
        pos = self.player.getPos()

        pivotNode = self.render.attachNewNode("env-pivot")
        pivotNode.setPos(pos)
        self.env.wrtReparentTo(pivotNode)


        if keyMap['left']:
            pivotNode.setHpr(-self.rotationSpeed * dt, 0, 0)
        if keyMap['right']:
            pivotNode.setHpr(self.rotationSpeed * dt, 0, 0)
        if keyMap['up']:
            pos.y += self.speed * dt
        if keyMap['down']:
            pos.y -= self.speed * dt

        self.player.setPos(pos)

        return task.cont

    def followPlayer(self, task):
        """
        followPlayer
        """
        pos = self.player.getPos()
        pos.z += 20
        self.camera.setPos(0, self.player.getY()-100, 50)
        self.camera.lookAt(pos)

        return task.cont

    def soundTask(self, task):
        if self.envSound.status() == self.envSound.READY:
            self.envSound.play()

        if self.wood.status() == self.wood.READY and flags['moving']:
            self.wood.play()
        elif self.wood.status() == self.wood.PLAYING and not flags['moving']:
            self.wood.stop()

        if self.wood2.status() == self.wood2.READY and flags['rotating']:
            self.wood2.play()
        elif self.wood2.status() == self.wood2.PLAYING and not flags['rotating']:
            self.wood2.stop()

        return task.cont

if __name__ == '__main__':
    game = Game()
    game.run()
